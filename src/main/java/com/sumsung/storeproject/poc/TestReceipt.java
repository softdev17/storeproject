/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumsung.storeproject.poc;

import dao.ReceiptDao;
import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author sumsung
 */
public class TestReceipt {
    public static void main(String[] args) {
      Product p1 = new Product(1,"Espresso",30);
        Product p2 = new Product(2,"Mocca",30);
        User seller = new User(1,"sung","06111111111");
        Customer customer = new Customer(1,"yayaa","0888888888881");
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDatail(p1, 1);
        receipt.addReceiptDatail(p2, 3);
        ReceiptDao dao = new ReceiptDao();
        dao.add(receipt);
    }
}
