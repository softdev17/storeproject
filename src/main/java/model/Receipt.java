/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author sumsung
 */
public class Receipt {

    private int id;
    private Date created;
    private Customer customer;
    private User seller;
    private ArrayList<ReceipDetail> receiptDetail;

    public Receipt(int id, Date created, User seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        receiptDetail = new ArrayList<>();
    }

    public Receipt(User seller, Customer customer) {
        this(-1, null, seller, customer);
    }

    public void addReceiptDatail(int id, Product product, int amount, double price) {
        for(int row = 0 ; row<receiptDetail.size();row++){
            ReceipDetail r = receiptDetail.get(row);
            if(r.getProduct().getId()==product.getId()){
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceipDetail(id, product, amount, price, this));
    }

    public void addReceiptDatail(Product product, int amount) {
        addReceiptDatail(-1, product, amount,product.getPrice());
    }
    
    public void deleteReceiptDetail(int row){
        receiptDetail.remove(row);
    }
    public double getTotal() {
        double total = 0;
        for (ReceipDetail r : receiptDetail) {
            total = total + r.getTotal();
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public ArrayList<ReceipDetail> getReceipDetail() {
        return receiptDetail;
    }

    public void setReceipDetail(ArrayList<ReceipDetail> receipDetail) {
        this.receiptDetail = receipDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id 
               + ", created=" + created 
               + ", seller=" + seller 
               + ", customer=" + customer 
               +" total=" + this.getTotal()
               + "\n}";
        for(ReceipDetail r : receiptDetail){
            str = str + r.toString()+"\n";
        }
        return str;
    }
}
